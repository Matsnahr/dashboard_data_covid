<?php

$curl = curl_init();
curl_setopt($curl,CURLOPT_URL, 'https://api.covid19api.com/summary');
curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
$result = curl_exec($curl);
curl_close($curl);

$result = json_decode($result,true);

$global = $result['Global'];
$temp_NewConfirmed = (string)$global['NewConfirmed'];
$TotalConfirmed = (string)$global['TotalConfirmed'];
$temp_NewDeaths = (string)$global['NewDeaths'];
$TotalDeaths = (string)$global['TotalDeaths'];
$temp_NewRecovered = (string)$global['NewRecovered'];
$TotalRecovered = (string)$global['TotalRecovered'];

$countries =$result['Countries'];


$tem_ngr=[];
    foreach($countries as $key => $value){
        $tem_ngr[]=$value['Country'];
    }

foreach ($countries as $key => $value_x) {
  $value_x['TotalConfirmed'];
}

foreach ($countries as $key => $value_a) {
  $value_a['TotalDeaths'];
  // exit;
}

  $tem_kon=[];
    foreach ($countries as $key => $value) {
        $tem_kon[]=$value['TotalConfirmed'];
    };


  $tem_color=[];
  $color=function(){
    $r=rand(0,255);
    $g=rand(0,255);
    $b=rand(0,255);
    return "rgb(".$r.",".$g.",".$b.")";
  };

  foreach($countries as $k => $v){
    for($x=1; $x<=count($v); $x++){
      $tem_color[]=$color();
    }
  }
?>

<!doctype html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js"
integrity="sha512-d9xgZrVZpmmQlfonhQUvTR7lMPtO7NkZMkA0ABN3PHCbKA5nqylQ/yWlFAyY6hYgdF1Qh6nYiuADWwKB4C2WSw=="
crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
<title>Update Date covid</title>

<style>

.country{
  overflow: scroll;
  height: 500px;
}

.label-counter {
  text-align: center;
  background-color: #f5f5f5;
  color: #777;
  font-size: 19px;
  text-transform: uppercase;
  line-height: 40px;
  font-family: roboto condensed,sans-serif;
}
#maincounter-wrap {
  margin: auto;
  text-align: center;
  padding-top: 20px;
}
#maincounter-wrap h1 {
  font-family: roboto condensed,sans-serif;
  font-weight: 300;
  font-size: 40px;
  color: #555;
  margin: 0;
}
.maincounter-number {
  font-size: 40px;
  font-weight: 700;
  color: #696969;
}

</style>

</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
<a class="navbar-brand" href="#">
<img src="unnamed.png" width="90" height="50" alt="" loading="lazy">
Coronavirus in the world
</a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
<span class="navbar-toggler-icon"></span>
</button>
</nav>

<div class="label-counter" id="page-top">Coronavirus Pandemic</div>
<div class="container-fluid">
<div class="row">
<div class="col-2">
<div class="card-header bg-secondary text-light">
Negara
</div>
<div class="country">
<?php foreach($countries as $key=>$value):?>
  <ul  class="navbar-nav mr-auto">
  <li class="nav-item dropdown">
  <a class="nav-link dropdown-toggle text-dark" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
  <?php echo $value['Country'];?>
  </a>
  <div class="dropdown-menu" aria-labelledby="navbarDropdown" > 
  <p class="dropdown-item">terkonfirmasi:<?php echo $value['TotalConfirmed'];?></p>
  <p class="dropdown-item">sembuh:<?php echo $value['TotalRecovered'];?></p>
  <p class="dropdown-item">meninggal:<?php echo $value['TotalDeaths'];?></p>
  </div>  
  </li>
  </ul>
  <?php endforeach;?>
  </div>
  </div>
<div class="col-8 ">
<canvas id="MyChart"></canvas>
</div>
  
  <div class="col-2 col ">
    <div id="maincounter-wrap" style="margin-top: 15px;" >
      <h3>Total Coronavirus Cases:</h3>
      <div class="maincounter-number">
        <span style="color: #aaa;"><?php echo number_format($TotalConfirmed);?> </span>
      </div>
    </div>
  <div id="maincounter-wrap" style="margin-top: 15px;" >
    <h3>Total Deaths:</h3>
    <div class="maincounter-number">
  <span style="color: #aaa;"><?php echo number_format($TotalDeaths);?></span>
</div>
  </div>
  <div id="maincounter-wrap" style="margin-top: 15px;" >
  <h3>Total Recovered:</h3> 
  <div class="maincounter-number">
    <span style="color: #8ACA2B;"><?php echo number_format($TotalRecovered);?></span>
  </div>
</div>
</div>

  
  <script>
  var ctx = document.getElementById('MyChart').getContext('2d');
  var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
      labels: <?php echo json_encode($tem_ngr); ?>,
      datasets: [{
        label: '# of Votes',
        data: <?php echo json_encode($tem_kon); ?>,
        backgroundColor: <?php echo json_encode($tem_color); ?>,
      }]
    },
    options: {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      }
    }
  });
  </script>
  
  <!-- Optional JavaScript; choose one of the two! -->
  
  <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

  </body>
  </html>